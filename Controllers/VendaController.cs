using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendaController : ControllerBase
    {
        private static List<Venda> vendas = new List<Venda>();
        private static int ProximoId = 1;
        
        [HttpPost]
        public IActionResult NovaVenda(Venda venda)
        {
            if(venda.itens.Count == 0)
                return BadRequest("A venda deve conter pelo menos 1 item");

            venda.Id = ProximoId++;
            venda.Status = "Aguardando Pagamento";
            vendas.Add(venda);
            return Ok(venda);
        }

        [HttpGet("{id}")]
        public IActionResult BuscarVenda(int id)
        {
            var venda = vendas.FirstOrDefault(x => x.Id == id);
            if(venda == null) 
            {
                return NotFound();
            }
            return Ok(venda);
        }

        [HttpPut("{id}")]
        public IActionResult AtualizarStatusVenda(int id, string novoStatus)
        {
            var venda = vendas.FirstOrDefault(x => x.Id == id);
            if(venda == null)
            {
                return NotFound();
            }
            else if(venda.Status == "Aguardando Pagamento" && novoStatus == "Pagamento Aprovado")
            {
                venda.Status = "Pagamento Aprovado";
            }
            else if(venda.Status == "Aguardando Pagamento" && novoStatus == "Cancelada")
            {
                venda.Status = "Cancelada";
            }
            else if(venda.Status == "Aguardando Aprovado" && novoStatus == "Enviado para Transporadora")
            {
                venda.Status = "Enviado para Transportadora";
            }
            else if(venda.Status == "Aguardando Aprovado" && novoStatus == "Cancelada")
            {
                venda.Status = "Cancelada";
            }
            else if(venda.Status == "Enviado para Transportadora" && novoStatus == "Entregue")
            {
                venda.Status = "Entregue";
            }
            else
            {
                return BadRequest("Transiçao de status inválida.");
            }
            return Ok(venda);
        }
    }
}